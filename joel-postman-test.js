// Generate the signature
function generateSignature() {
  var secretUTF8   = crypto.enc.Utf8.parse(merchantSecret);
  var signature    = crypto.HmacSHA256(payloadString, secretUTF8);
  var signatureB64 = crypto.enc.Base64.stringify(signature);
  var payloadSignatureEnc = encodeURIComponent(signatureB64);
}

function encryptCard() {
  var cardString = JSON.stringify(card, null, 2);
  var cardEnc = encodeURIComponent(cardString);
  console.log('cardEnc: ' + cardEnc);
}

function setPostmanEnv() {
  var secretUTF8   = crypto.enc.Utf8.parse(merchantSecret);
  var signature    = crypto.HmacSHA256(payloadString, secretUTF8);
  var signatureB64 = crypto.enc.Base64.stringify(signature);
  var payloadSignatureEnc = encodeURIComponent(signatureB64);
}
